cd `dirname $0`/rhizom
for locale in `ls locale`; do
    ../manage.py makemessages -l $locale -i templates/account -i templates/socialaccount -i templates/openid
done
../manage.py compilemessages
