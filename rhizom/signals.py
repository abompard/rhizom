from allauth.account.signals import user_logged_in
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver

from .models.profile import Profile

import logging
logger = logging.getLogger(__name__)


def create_profile(user):
    if not Profile.objects.filter(user=user).exists():
        Profile.objects.create(user=user)


# Create a Profile when a User is created or logs in.
@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def on_user_save(sender, **kwargs):
    create_profile(kwargs["instance"])


@receiver(user_logged_in)
def on_user_logged_in(sender, **kwargs):
    # Sent when a user logs in.
    create_profile(kwargs["user"])
