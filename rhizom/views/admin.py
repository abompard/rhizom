import json

from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import PermissionDenied
from django.http import (
    HttpResponse, HttpResponseRedirect, QueryDict, JsonResponse)
from django.shortcuts import get_object_or_404, redirect
from django.template import loader
from django.urls import reverse
from django.utils.translation import ugettext as _
from django.views.generic import View
from django.views.generic.edit import UpdateView

from rhizom.forms import GraphProperties, NewAccess, RelationshipTypes
from rhizom.models.graph import Graph
from rhizom.models.permission import Permission, get_perm_choices
from rhizom.models.person import Person
from rhizom.models.relationship import RelationshipType


class GraphAdmin(UpdateView):

    template_name = "rhizom/graph_admin.html"
    model = Graph
    pk_url_kwarg = "graph"
    context_object_name = "graph"
    form_class = GraphProperties

    def get_object(self):
        obj = super().get_object()
        if not obj.allowed_for(self.request.user, "admin"):
            raise PermissionDenied
        return obj

    def get_success_url(self):
        return reverse("rhizom:graph_admin", kwargs={"graph": self.object.pk})

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update(dict(instance=self.object, prefix="graphprops"))
        return kwargs

    def get_form(self):
        form = super().get_form()
        form.fields["center"].queryset = Person.objects.filter(
            graph=self.object)
        return form

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

    def form_valid(self, form):
        messages.success(self.request, _("Graph updated."))
        return super().form_valid(form)

    def post(self, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form()
        # Graph: properties edition
        if self.request.POST.get("formname") == "graphprops":
            if form.is_valid():
                return self.form_valid(form)
            else:
                return self.form_invalid(form)
        # Graph: import
        if self.request.POST.get("formname") == "graphimport":
            if "import" not in self.request.FILES:
                messages.error(self.request, _("No graph data."))
                return HttpResponseRedirect(self.get_success_url())
            try:
                data = json.loads(
                    self.request.FILES["import"].read().decode("utf-8"))
            except ValueError as e:
                messages.error(
                    self.request,
                    _("Invalid graph data: %(error)s", {"error": str(e)}))
                return HttpResponseRedirect(self.get_success_url())
            self.object.import_dict(data)
            self.object.save()
            messages.success(self.request, _("Graph imported."))
            return HttpResponseRedirect(self.get_success_url())
        # Graph: deletion
        if self.request.POST.get("formname") == "graphdel":
            self.object.delete()
            messages.success(self.request, _("Graph deleted."))
            return redirect("rhizom:index")
        return self.render_to_response(self.get_context_data(form=form))


class GraphDynamicTable(View):

    template_name = None
    model = None
    form_class = None
    messages = {
        "added": None,
        "updated": None,
        "deleted": None,
    }
    replace_on_delete = False

    def _get_graph(self):
        graph = get_object_or_404(Graph, pk=int(self.kwargs["graph"]))
        if not graph.allowed_for(self.request.user, "admin"):
            raise PermissionDenied
        return graph

    def _get_instance(self):
        return get_object_or_404(
            self.model,
            graph=self.graph,
            pk=int(self.kwargs["pk"])
            )

    def get_context_data(self, form):
        return dict(graph=self.graph, form=form)

    def _get_content(self, form):
        return loader.render_to_string(
            self.template_name, self.get_context_data(form), self.request)

    def form_invalid(self, form):
        return JsonResponse(
            {"status": "error", "action": "replace-table",
             "content": self._get_content(form)})

    def get(self, *args, **kwargs):
        self.graph = self._get_graph()
        form = self.form_class(graph=self.graph)
        return HttpResponse(self._get_content(form))

    def post(self, *args, **kwargs):
        self.graph = self._get_graph()
        form = self.form_class(self.request.POST, graph=self.graph)
        if not form.is_valid():
            return self.form_invalid(form)
        form.save()
        form = self.form_class(graph=self.graph)  # get a blank form
        return JsonResponse(
            {"status": "OK", "action": "replace-table",
             "content": self._get_content(form),
             "message": self.messages["added"]})

    def put(self, *args, **kwargs):
        self.graph = self._get_graph()
        instance = self._get_instance()
        put_data = QueryDict(self.request.body, encoding=self.request.encoding)
        form = self.form_class(
            put_data, graph=self.graph, instance=instance)
        if not form.is_valid():
            return self.form_invalid(form)
        form.save()
        form = self.form_class(graph=self.graph)  # get a blank form
        return JsonResponse(
            {"status": "OK", "action": "replace-table",
             "content": self._get_content(form),
             "message": self.messages["updated"]})

    def delete(self, *args, **kwargs):
        self.graph = self._get_graph()
        instance = self._get_instance()
        instance.delete()
        response = {"status": "OK", "message": self.messages["deleted"]}
        if self.replace_on_delete:
            response["action"] = "replace-table"
            form = self.form_class(graph=self.graph)  # get a blank form
            response["content"] = self._get_content(form)
        else:
            response["action"] = "delete"
        return JsonResponse(response)


class GraphRelType(GraphDynamicTable):

    template_name = "rhizom/graph_admin/edit_reltypes.html"
    model = RelationshipType
    form_class = RelationshipTypes
    messages = {
        "added": _("Relationship type added."),
        "updated": _("Relationship type modified."),
        "deleted": _("Relationship type removed."),
    }


class GraphAccess(GraphDynamicTable):

    template_name = "rhizom/graph_admin/edit_accesses.html"
    model = Permission
    form_class = NewAccess
    messages = {
        "added": _("Access added."),
        "updated": _("Access modified."),
        "deleted": _("Access removed."),
    }
    replace_on_delete = True

    def get_context_data(self, form):
        context = super().get_context_data(form)
        context["perm_choices"] = get_perm_choices()
        context["has_public_access"] = Permission.objects.filter(
            graph=self.graph, user=None).exists()
        return context


class FindUsername(LoginRequiredMixin, View):

    def get(self, *args, **kwargs):
        User = get_user_model()
        query = self.request.GET.get("q")
        if not query:
            usernames = []
        else:
            usernames = User.objects.filter(
                username__icontains=query
                ).values_list("username", flat=True)
        return JsonResponse(list(usernames), safe=False)
