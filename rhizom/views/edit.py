from django.core.exceptions import PermissionDenied
from django.db.models import Q
from django.http import JsonResponse, QueryDict
from django.shortcuts import get_object_or_404
from django.template import loader
from django.utils.timezone import now
from django.utils.translation import ugettext as _
from django.views.generic import TemplateView, View
from django.views.generic.detail import DetailView

from rhizom.models.graph import Graph
from rhizom.models.person import Person
from rhizom.models.relationship import Relationship
from rhizom.forms import NewRelationship, RelationshipForm


class GraphEditAll(DetailView):

    template_name = "rhizom/graph_edit.html"
    model = Graph
    pk_url_kwarg = "graph"
    context_object_name = "graph"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        newrel_form = NewRelationship(graph=self.object)
        context.update(dict(newrel_form=newrel_form))
        return context

    def get(self, *args, **kwargs):
        self.object = self.get_object()
        if not self.object.allowed_for(self.request.user, "edit"):
            raise PermissionDenied
        context = self.get_context_data(object=self.object)
        return self.render_to_response(context)


class GraphEditPerson(TemplateView):

    template_name = "rhizom/graph_edit.html"
    model = Graph
    context_object_name = "graph"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        graph = get_object_or_404(Graph, pk=int(self.kwargs["graph"]))
        if not graph.allowed_for(self.request.user, "edit"):
            raise PermissionDenied
        person = get_object_or_404(
            Person, pk=self.kwargs["person"], graph=graph)
        newrel_form = NewRelationship(graph=graph, person=person)
        relationships = []
        for rel in person.rels_as_source.all():
            rel.other = rel.target
            relationships.append(rel)
        for rel in person.rels_as_target.all():
            rel.other = rel.source
            relationships.append(rel)
        relationships.sort(key=lambda rel: rel.other.name)
        context.update(dict(
            graph=graph,
            newrel_form=newrel_form,
            person=person,
            relationships=relationships,
        ))
        return context


class GraphRelAdd(View):

    template_name = "rhizom/graph_edit/add_rel.html"

    def post(self, *args, **kwargs):
        graph = get_object_or_404(Graph, pk=int(self.kwargs["graph"]))
        if not graph.allowed_for(self.request.user, "edit"):
            raise PermissionDenied
        try:
            person = int(self.request.POST["person"])
        except (KeyError, ValueError):
            person = None
        else:
            person = get_object_or_404(Person, pk=person)
        newrel_form = NewRelationship(
            self.request.POST, graph=graph, person=person)
        if not newrel_form.is_valid():
            content = loader.render_to_string(
                "rhizom/graph_edit/add_rel.html",
                {"graph": graph, "newrel_form": newrel_form, "person": person},
                self.request)
            return JsonResponse(
                {"status": "error", "action": "replace", "content": content})
        source, _created = Person.objects.get_or_create(
            graph=graph, name=newrel_form.cleaned_data["source"])
        target, _created = Person.objects.get_or_create(
            graph=graph, name=newrel_form.cleaned_data["target"])
        if source.pk > target.pk:
            source, target = target, source  # source is always the lowest id
        if Relationship.objects.filter(
                source=source, target=target,
                reltype__pk=newrel_form.cleaned_data["rtype"]).exists():
            content = loader.render_to_string(
                "rhizom/graph_edit/add_rel.html",
                {"graph": graph, "newrel_form": newrel_form, "person": person},
                self.request)
            return JsonResponse(
                {"status": "error", "action": "replace", "content": content,
                 "message": _("This relationship already exists.")})
        newlink = Relationship.objects.create(
            source=source, target=target,
            reltype_id=newrel_form.cleaned_data["rtype"],
            dotted=newrel_form.cleaned_data["dotted"])
        graph.last_modification = now()
        graph.save()
        result = {"status": "OK", "message": _("Relationship added.")}
        if person:
            if source.pk == person.pk:
                newlink.other = newlink.target
            elif target.pk == person.pk:
                newlink.other = newlink.source
            content = loader.render_to_string(
                "rhizom/graph_edit/edit_rel.html",
                {"graph": graph, "rel": newlink}, self.request)
            result.update({"action": "add", "content": content})
        else:
            newrel_form = NewRelationship(graph=graph)  # Get a clean instance
            content = loader.render_to_string(
                "rhizom/graph_edit/new_rel.html",
                {"graph": graph, "newrel_form": newrel_form},
                self.request)
            result.update({"action": "replace", "content": content})
        return JsonResponse(result)


class GraphRelEdit(View):

    def _get_rel(self):
        self.graph = get_object_or_404(Graph, pk=int(self.kwargs["graph"]))
        if not self.graph.allowed_for(self.request.user, "edit"):
            raise PermissionDenied
        return get_object_or_404(Relationship, pk=int(self.kwargs["rel"]))

    def put(self, *args, **kwargs):
        rel = self._get_rel()
        put_data = QueryDict(self.request.body, encoding=self.request.encoding)
        form = RelationshipForm(put_data, instance=rel)
        if not form.is_valid():
            messages = []
            for fieldname, errors in form.errors.items():
                for error in errors:
                    messages.append("{}: {}".format(
                        form.fields[fieldname].label, error))
            return JsonResponse({
                "status": "error",
                "message": "<br/>".join(messages),
                })
        if not form.has_changed():
            return JsonResponse({
                "status": "OK",
                "message": _("No modification."),
                })
        form.save()
        self.graph.last_modification = now()
        self.graph.save()
        rel.other = Person.objects.get(pk=int(put_data["other"]))
        assert rel.other.pk in (rel.source.pk, rel.target.pk)
        content = loader.render_to_string(
            "rhizom/graph_edit/edit_rel.html",
            dict(graph=self.graph, rel=rel), self.request)
        return JsonResponse(dict(
                status="OK", action="replace", content=content,
                message=_("Relationship modified.")))

    def delete(self, *args, **kwargs):
        rel = self._get_rel()
        persons = (rel.source, rel.target)
        rel.delete()
        # Cleanup orphans
        for person in persons:
            if not Relationship.objects.filter(
                    Q(source=person) | Q(target=person)
                    ).exists():
                person.delete()
        self.graph.last_modification = now()
        self.graph.save()
        return JsonResponse(dict(
                status="OK", action="delete",
                message=_("Relationship removed.")))
