from urllib.parse import quote

from django.contrib.staticfiles import finders as static_finders
from django.core.exceptions import PermissionDenied
from django.db.models import Q
from django.http import HttpResponse, JsonResponse, HttpResponseBadRequest
from django.shortcuts import redirect
from django.views.generic.detail import DetailView, BaseDetailView
from django.urls import reverse
from django.utils.timezone import now

from rhizom.models.graph import Graph
from rhizom.models.relationship import Relationship
from rhizom.lib import compute_node_circles, compute_node_branch

try:
    from lxml import etree as ET
except ImportError:
    from xml.etree import ElementTree as ET


class GraphView(DetailView):

    template_name = "rhizom/graph_view.html"
    model = Graph
    pk_url_kwarg = "graph"
    context_object_name = "graph"

    def get(self, *args, **kwargs):
        self.object = self.get_object()
        if not self.object.allowed_for(self.request.user, "view"):
            raise PermissionDenied
        if (not self.request.user.is_authenticated or
                not self.request.user.is_superuser):
            # Admins don't count as visitors, so they can list old graphs and
            # still visit them without touching them.
            self.object.last_access = now()
            self.object.save()
        context = self.get_context_data(object=self.object)
        return self.render_to_response(context)


class GraphData(BaseDetailView):

    model = Graph
    pk_url_kwarg = "graph"
    context_object_name = "graph"

    def get(self, *args, **kwargs):
        graph = self.object = self.get_object()
        if not graph.allowed_for(self.request.user, "view"):
            raise PermissionDenied
        graph_data = {"nodes": [], "links": [], "center": None,
                      "anonymous": self.object.anonymous}
        node_index = {}
        for index, person in enumerate(graph.person_set.order_by("pk")):
            node = {
                    "id": person.pk,
                    "edit_url": reverse(
                        "rhizom:graph_edit_person",
                        kwargs=dict(graph=graph.pk, person=person.pk)),
                    }
            if graph.center_id == person.id:
                node.update({"size": 15, "fixed": True, "center": True})
                graph_data["center"] = index
            node["name"] = person.name
            graph_data["nodes"].append(node)
            node_index[person.id] = index
        for rel in Relationship.objects.filter(
                Q(source__graph=graph) | Q(target__graph=graph)
                ).all():
            link = {"source": node_index[rel.source_id],
                    "target": node_index[rel.target_id],
                    "css": rel.reltype.cssname.lower(),
                    "dotted": rel.dotted,
                    "bidirect": rel.bidirect,
                    }
            graph_data["links"].append(link)
        # Compute link siblings
        siblings = {}
        for link in graph_data["links"]:
            siblings_count = len([
                l for l in graph_data["links"] if l["source"] == link["source"]
                and l["target"] == link["target"]
                ])
            link["siblings"] = siblings_count
            if link["siblings"] > 0:
                # compute sibling_id
                if (link["source"], link["target"]) in siblings:
                    siblings[(link["source"], link["target"])] += 1
                    sibling_id = siblings[(link["source"], link["target"])]
                elif (link["target"], link["source"]) in siblings:
                    siblings[(link["target"], link["source"])] += 1
                    sibling_id = siblings[(link["target"], link["source"])]
                else:
                    siblings[(link["source"], link["target"])] = sibling_id = 0
                link["sibling_id"] = sibling_id
        if graph_data["center"] is not None:
            compute_node_circles(
                graph_data["nodes"], graph_data["links"], graph_data["center"])
            compute_node_branch(graph_data["nodes"], graph_data["links"])
        return JsonResponse(graph_data)


class GraphExport(BaseDetailView):

    model = Graph
    pk_url_kwarg = "graph"
    context_object_name = "graph"

    def get(self, *args, **kwargs):
        self.object = self.get_object()
        if not self.object.allowed_for(self.request.user, "view"):
            raise PermissionDenied
        if self.kwargs["format"] == "json":
            return self.to_json()
        if self.kwargs["format"] == "svg":
            # This must be called with POST
            return redirect("rhizom:graph_view", graph=self.object.pk)
        raise HttpResponseBadRequest("The URL is wrong")

    def post(self, *args, **kwargs):
        self.object = self.get_object()
        if not self.object.allowed_for(self.request.user, "view"):
            raise PermissionDenied
        if self.kwargs["format"] == "svg":
            return self.to_svg()

    def to_svg(self):
        svg = ET.fromstring(self.request.POST["svg"].strip().encode("utf-8"))
        # cleanups
        for attr in ("width", "height"):
            elems = svg.findall(".//*[@%s='100%%']" % attr) \
                  + svg.findall(".[@%s='100%%']" % attr)
            for elem in elems:
                del elem.attrib[attr]
        for elem in svg.findall(".//*[@class='']"):
            del elem.attrib["class"]
        # ET.dump(svg)
        css = []
        css_file = static_finders.find("rhizom/css/graph.css")
        with open(css_file) as fh:
            css.append(fh.read())
        for rtype in self.object.relationshiptype_set.all():
            css.append("path.link.%s { stroke: %s; }" % (
                       rtype.cssname, rtype.color))
        style = ET.Element("style")
        style.text = "\n".join(css)
        svg.insert(0, style)
        return HttpResponse(
            ET.tostring(svg, encoding="utf-8"),
            content_type="image/svg+xml; charset=utf-8")

    def to_json(self):
        graph = self.object
        is_admin = graph.allowed_for(self.request.user, "admin")
        data = graph.export_dict(with_perms=is_admin)
        response = JsonResponse(
            data, json_dumps_params=dict(sort_keys=True, indent=2))
        user_agent = self.request.META.get('HTTP_USER_AGENT')
        if user_agent and 'WebKit' in user_agent:
            # WebKit filename i18n requires setting a binary header binary
            # isn't supported by Django.
            filename = ""
        elif user_agent and 'MSIE' in user_agent:
            # MSIE does not support internationalized filenames at all
            filename = ""
        else:
            filename = "filename*=UTF-8''{}.json".format(quote(
                graph.name.encode('utf-8')))
        response["Content-Disposition"] = "attachment; {}".format(filename)
        return response
