from django.contrib.auth.mixins import AccessMixin
from django.urls import reverse, reverse_lazy
from django.utils.translation import ugettext_lazy as _
from django.views.generic.list import MultipleObjectMixin

from rhizom.forms import NewGraph, CopyGraph
from rhizom.models.graph import Graph
from rhizom.models.permission import Permission, PermissionLevel
from rhizom.models.relationship import RelationshipType
from rhizom.views.generic import MultipleFormView


class IndexView(AccessMixin, MultipleObjectMixin, MultipleFormView):

    template_name = "rhizom/index.html"
    form_classes = {
        "new": NewGraph,
        "copy": CopyGraph,
    }
    success_url = reverse_lazy("rhizom:index")
    paginate_by = 20
    allow_empty = True
    messages = {
        "new": _("Graph created"),
        "copy": _("Graph copied"),
    }

    def get_queryset(self):
        if self.request.user.is_superuser:
            graphs = Graph.objects.all()
        else:
            # Get public graphs
            graphs = Graph.objects.filter(
                permission__user__isnull=True,
                permission__level__gte=PermissionLevel.view.value)
            if self.request.user.is_authenticated:
                # add private graphs
                private_graphs = Graph.objects.filter(
                    permission__user=self.request.user,
                    permission__level__gte=PermissionLevel.view.value)
                graphs = graphs | private_graphs
        return graphs.distinct().order_by("pk")

    def get_forms(self):
        forms = super(IndexView, self).get_forms()
        forms["copy"].fields["existing"].choices = [
            (str(graph.id), graph.name) for graph in self.object_list]
        return forms

    def get_success_url(self, form_name=None, instance=None):
        if form_name is not None and instance is not None:
            return reverse("rhizom:graph_admin", kwargs={"graph": instance.pk})
        return super().get_success_url()

    def post_save(self, form_name, instance):
        if form_name == "new":
            # Set the user as admin
            Permission.objects.create(
                graph=instance, user=self.request.user,
                level=PermissionLevel.admin.value
                )
            # Create an initial relationship type
            RelationshipType.objects.create(
                graph=instance, name=_("Link"), color="#000000")
        elif form_name == "copy":
            # Set the user as admin
            try:
                myperm = Permission.objects.get(
                    graph=instance, user=self.request.user)
            except Permission.DoesNotExist:
                myperm = Permission(graph=instance, user=self.request.user)
            myperm.level = PermissionLevel.admin.value
            myperm.save()

    def get_context_data(self, **kwargs):
        """
        Insert the form into the context dict.
        """
        context = super().get_context_data(**kwargs)
        context["graphs"] = context["page_obj"] or context["object_list"]
        return context

    def get(self, request, *args, **kwargs):
        self.object_list = self.get_queryset()
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated:
            return self.handle_no_permission()
        self.object_list = self.get_queryset()
        return super().post(request, *args, **kwargs)
