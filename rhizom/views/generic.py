from django.contrib import messages
from django.core.exceptions import SuspiciousOperation
from django.http import HttpResponseRedirect
from django.views.generic.base import TemplateResponseMixin
from django.views.generic.edit import FormMixin, ProcessFormView


class MultipleFormMixin(FormMixin):
    """
    A mixin that provides a way to show and handle multiple forms in a request.
    """

    initial = {}
    form_classes = None
    success_url = None
    prefixes = None
    messages = {}

    def get_initial(self):
        """
        Returns the initial data to use for forms on this view.
        """
        return self.initial.copy()

    def get_prefixes(self):
        """
        Returns the prefixes to use for forms on this view
        """
        return self.prefix

    def get_form_classes(self):
        """
        Returns the form classes to use in this view
        """
        return self.form_classes

    def get_forms(self, form_classes=None):
        """
        Returns instances of the forms to be used in this view.
        """
        if form_classes is None:
            form_classes = self.get_form_classes()
        form_kwargs = self.get_form_kwargs()
        return dict(
            (form_name, form_class(**form_kwargs[form_name]))
            for form_name, form_class in form_classes.items()
            )

    def get_form_kwargs(self):
        """
        Returns the keyword arguments for instantiating the forms.
        """
        initial = self.get_initial()
        prefixes = self.get_prefixes()
        kwargs = {}
        for form_name in self.get_form_classes():
            kwargs[form_name] = {}
            if initial and form_name in initial:
                kwargs[form_name]["initial"] = initial[form_name]
            if prefixes and form_name in prefixes:
                kwargs[form_name]["prefix"] = prefixes[form_name]
            if (self.request.method == 'POST' and
                    self.request.POST["formname"] == form_name):
                kwargs[form_name].update({
                    'data': self.request.POST,
                    'files': self.request.FILES,
                })
        return kwargs

    def form_valid(self, form_name, form):
        """
        If the form is valid, redirect to the supplied URL.
        """
        if hasattr(form, "save"):
            instance = form.save()
            self.post_save(form_name, instance)
            msg = self.messages.get(form_name)
            if msg is not None:
                messages.success(self.request, msg)
            return HttpResponseRedirect(
                self.get_success_url(form_name, instance))
        else:
            return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form_name, form):
        """
        If the form is invalid, re-render the context data with the
        data-filled form and errors.
        """
        return self.render_to_response(
            self.get_context_data(**{form_name: form}))

    def post_save(self, form_name, instance):
        # Called after calling .save() on a form
        pass

    def get_success_url(self, form_name=None, instance=None):
        return super(MultipleFormMixin, self).get_success_url()

    def get_context_data(self, **kwargs):
        """
        Insert the form into the context dict.
        """
        forms = self.get_forms()
        for form_name, form in forms.items():
            if form_name not in kwargs:
                kwargs["{}_form".format(form_name)] = form
        # Don't call our super because it will read self.form and it's None.
        # Call our super's super:
        return super(FormMixin, self).get_context_data(**kwargs)


class ProcessMultipleFormView(ProcessFormView):
    """
    A mixin that renders multiple forms on GET and process them on POST.
    """
    def get(self, request, *args, **kwargs):
        """
        Handles GET requests and instantiates blank versions of the forms.
        """
        return self.render_to_response(self.get_context_data())

    def post(self, request, *args, **kwargs):
        """
        Handles POST requests, instantiating the forms with the passed
        POST variables and then checking for validity.
        """
        forms = self.get_forms()
        if not self.request.POST.get("formname"):
            raise SuspiciousOperation
        form_name = self.request.POST["formname"]
        form = forms[form_name]
        if form.is_valid():
            return self.form_valid(form_name, form)
        else:
            return self.form_invalid(form_name, form)


class MultipleFormView(
        TemplateResponseMixin, MultipleFormMixin, ProcessMultipleFormView):
    """
    A view for displaying multiple forms, and rendering a template response.
    """
