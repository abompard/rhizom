from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic.edit import UpdateView

from rhizom.forms import UserProfileForm


class ProfileView(LoginRequiredMixin, UpdateView):

    template_name = "rhizom/profile/profile.html"
    model = get_user_model()
    form_class = UserProfileForm
    success_url = reverse_lazy("rhizom:profile")

    def get_object(self):
        return self.request.user

    def get_initial(self):
        initial = super().get_initial()
        initial["locale"] = (
            self.object.rhizom_profile.locale or self.request.LANGUAGE_CODE)
        return initial

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        return kwargs
