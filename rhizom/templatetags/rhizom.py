from django import template
from django.shortcuts import resolve_url

register = template.Library()


@register.inclusion_tag('rhizom/nav/nav_link.html',
                        takes_context=True)
def nav_link(context, view_name, label, icon=None, next=None, **kwargs):
    url = resolve_url(view_name, **kwargs)
    is_active = (context["request"].path == url)
    context = dict(
        url=url,
        label=label,
        icon=icon,
        is_active=is_active,
        )
    if next:
        context["query_string"] = "next={}".format(next)
    return context


def _has_graph_perm(user, graph, perm):
    return user.has_perm("rhizom.{}_graph".format(perm), graph)


@register.filter
def has_graph_view_perm(user, graph):
    return _has_graph_perm(user, graph, "view")


@register.filter
def has_graph_edit_perm(user, graph):
    return _has_graph_perm(user, graph, "edit")


@register.filter
def has_graph_admin_perm(user, graph):
    return _has_graph_perm(user, graph, "admin")
