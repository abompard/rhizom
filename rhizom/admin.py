from django.contrib import admin
from rhizom.models import graph, permission, person, profile, relationship

admin.site.register(permission.Permission)
admin.site.register(person.Person)
admin.site.register(profile.Profile)
admin.site.register(relationship.Relationship)
admin.site.register(relationship.RelationshipType)


@admin.register(graph.Graph)
class GraphAdmin(admin.ModelAdmin):
    date_hierarchy = "creation"
    list_display = ("name", "creation", "last_modification", "last_access")
