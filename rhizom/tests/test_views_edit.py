import datetime
import json
from urllib.parse import urlencode

from django.urls import reverse
from django.utils.timezone import now, utc

from .utils import TestCase
from ..models.person import Person
from ..models.relationship import Relationship, RelationshipType


class GraphEditionTestCase(TestCase):

    def setUp(self):
        self.graph = self.create_graph_and_login()
        self.reltype_1 = RelationshipType.objects.create(
            graph=self.graph, name="reltype 1", color="#000000")
        self.graph.last_modification = datetime.datetime(
            2015, 1, 1, 0, 0, 0, tzinfo=utc)
        self.graph.save()

    def test_graph_add_new(self):
        post_data = {
            "source": "Person 1",
            "target": "Person 2",
            "rtype": self.reltype_1.pk,
        }
        url = reverse("rhizom:graph_edit_relationship_add", kwargs=dict(
                      graph=self.graph.pk))
        response = self.client.post(url, data=post_data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(
            response.content.decode(response.charset))["status"], "OK")
        self.graph.refresh_from_db()
        self.assertTrue(self.graph.last_modification >
                        now() - datetime.timedelta(days=1))
        self.assertEqual(self.graph.relationships.count(), 1)
        rel = self.graph.relationships[0]
        self.assertEqual(rel.reltype.name, "reltype 1")
        self.assertEqual(rel.source.name, "Person 1")
        self.assertEqual(rel.target.name, "Person 2")

    def test_graph_edit(self):
        pers1 = Person.objects.create(graph=self.graph, name="Person 1")
        pers2 = Person.objects.create(graph=self.graph, name="Person 2")
        rel = Relationship.objects.create(
            source=pers1, target=pers2, reltype=self.reltype_1)
        newreltype = RelationshipType.objects.create(
            graph=self.graph, name="reltype 2", color="#FF0000")
        post_data = {
            "reltype": newreltype.pk,
            "dotted": "y",
            "other": pers2.pk,
        }
        url = reverse("rhizom:graph_edit_relationship_edit", kwargs=dict(
                      graph=self.graph.pk, rel=rel.pk))
        response = self.client.put(
            url, data=urlencode(post_data))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(
            response.content.decode(response.charset))["status"], "OK")
        self.graph.refresh_from_db()
        self.assertTrue(self.graph.last_modification >
                        now() - datetime.timedelta(days=1))
        self.assertEqual(self.graph.relationships.count(), 1)
        rel = self.graph.relationships[0]
        rel.refresh_from_db()
        self.assertEqual(rel.reltype.name, "reltype 2")
        self.assertEqual(rel.dotted, True)
        self.assertEqual(rel.source.name, "Person 1")
        self.assertEqual(rel.target.name, "Person 2")

    def test_graph_delete(self):
        pers1 = Person.objects.create(graph=self.graph, name="Person 1")
        pers2 = Person.objects.create(graph=self.graph, name="Person 2")
        rel = Relationship.objects.create(
            source=pers1, target=pers2, reltype=self.reltype_1)
        url = reverse("rhizom:graph_edit_relationship_edit", kwargs=dict(
                      graph=self.graph.pk, rel=rel.pk))
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(
            response.content.decode(response.charset))["status"], "OK")
        self.graph.refresh_from_db()
        self.assertTrue(self.graph.last_modification >
                        now() - datetime.timedelta(days=1))
        self.assertEqual(self.graph.relationships.count(), 0)
        self.assertEqual(self.graph.person_set.count(), 0)

    def test_graph_add_empty_person(self):
        # Don't crash on an empty "person" value.
        post_data = {
            "person": "",
            "source": "Person 1",
            "target": "Person 2",
            "rtype": self.reltype_1.pk,
        }
        url = reverse("rhizom:graph_edit_relationship_add", kwargs=dict(
                      graph=self.graph.pk))
        response = self.client.post(url, data=post_data)
        self.assertEqual(response.status_code, 200)
