from .utils import TestCase
from ..models.graph import Graph
from ..models.person import Person


class GraphTestCase(TestCase):

    def setUp(self):
        self.graph = Graph.objects.create(name="test")

    def test_copy_graph_with_center(self):
        graph2 = Graph.objects.create(name="test2")
        person = Person.objects.create(
            graph=graph2, name="person")
        graph2.center_id = person.id
        graph2.save()
        self.graph.clone(graph2)
        self.assertEqual(self.graph.person_set.count(), 1)
        person2 = self.graph.person_set.first()
        self.assertEqual(self.graph.center, person2)

    def test_copy_graph_without_center(self):
        graph2 = Graph.objects.create(name="test2")
        self.graph.clone(graph2)
        self.assertIsNone(self.graph.center)
