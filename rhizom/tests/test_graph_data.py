import json

from django.urls import reverse
from .utils import TestCase
from ..models.person import Person
from ..models.relationship import RelationshipType, Relationship


class GraphDataTestCase(TestCase):

    def test_siblings(self):
        graph = self.create_graph_and_login()
        person1 = Person.objects.create(graph=graph, name="person1")
        person2 = Person.objects.create(graph=graph, name="person2")
        person3 = Person.objects.create(graph=graph, name="person3")
        reltype1 = RelationshipType.objects.create(
            graph=graph, name="reltype1")
        reltype2 = RelationshipType.objects.create(
            graph=graph, name="reltype2")
        Relationship.objects.create(
            source=person1, target=person2, reltype=reltype1)
        Relationship.objects.create(
            source=person1, target=person2, reltype=reltype2)
        Relationship.objects.create(
            source=person2, target=person3, reltype=reltype1)
        response = self.client.get(
            reverse("rhizom:graph_data", kwargs=dict(graph=graph.pk)))
        data = json.loads(response.content.decode(response.charset))
        self.assertEqual(len(data["links"]), 3)
        self.assertIn(
            {
                'source': 0, 'target': 1, 'dotted': False, 'bidirect': True,
                'sibling_id': 0, 'siblings': 2, 'css': 'reltype1',
            },
            data["links"])
        self.assertIn(
            {
                'source': 0, 'target': 1, 'dotted': False, 'bidirect': True,
                'sibling_id': 1, 'siblings': 2, 'css': 'reltype2',
            },
            data["links"])
        self.assertIn(
            {
                'source': 1, 'target': 2, 'dotted': False, 'bidirect': True,
                'sibling_id': 0, 'siblings': 1, 'css': 'reltype1',
            },
            data["links"])
