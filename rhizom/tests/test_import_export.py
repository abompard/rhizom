import json

from django.contrib.auth.models import User
from django.urls import reverse

from .utils import TestCase
from ..models.graph import Graph
from ..models.person import Person
from ..models.permission import PermissionLevel


class ImportExportTestCase(TestCase):

    def test_import_export(self):
        graph = Graph.objects.create(name="test")
        User.objects.create_user(username="user1", email="user1@example.com")
        User.objects.create_user(username="user2", email="user2@example.com")
        User.objects.create_user(username="user3", email="user3@example.com")
        data = {
            "graph": {
                "name": "different name",
                "anonymous": True,
                "center.name": "person2",
            },
            "persons": [
                {"name": "person1"},
                {"name": "person2"},
                {"name": "person3"},
            ],
            "relationships": [
                {
                    "source.name": "person1",
                    "target.name": "person2",
                    "reltype.name": "type1",
                    "dotted": False,
                    "bidirect": True,
                },
                {
                    "source.name": "person2",
                    "target.name": "person3",
                    "reltype.name": "type2",
                    "dotted": True,
                    "bidirect": True,
                },
            ],
            "relationship_types": [
                {"name": "type1", "color": "#0000FF", },
                {"name": "type2", "color": "#FF0000", },
            ],
            "permissions": [
                {"user.username": "user1", "level": "admin"},
                {"user.username": "user2", "level": "edit"},
                {"user.username": "user3", "level": "view"},
            ],
            "version": 2,
        }
        graph.import_dict(data)
        graph.refresh_from_db()
        self.assertEqual(graph.name, "different name")
        self.assertTrue(graph.anonymous)
        self.assertEqual([p.name for p in graph.person_set.order_by("name")],
                         ["person1", "person2", "person3"])
        person1 = Person.objects.get(graph=graph, name="person1")
        person2 = Person.objects.get(graph=graph, name="person2")
        person3 = Person.objects.get(graph=graph, name="person3")
        self.assertEqual(graph.center, person2)
        self.assertEqual(
            [(rt.name, rt.color) for rt in
             graph.relationshiptype_set.order_by("name")],
            [("type1", "#0000FF"), ("type2", "#FF0000")]
            )
        self.assertEqual(
            [(r.source, r.target, r.reltype.name, r.dotted)
             for r in graph.relationships.order_by("source__pk")],
            [(person1, person2, "type1", False),
             (person2, person3, "type2", True)],
            )
        self.assertEqual(
            [(p.user.username, p.level) for p in
             graph.permission_set.order_by("user__username")],
            [("user1", PermissionLevel.admin.value),
             ("user2", PermissionLevel.edit.value),
             ("user3", PermissionLevel.view.value)]
            )

    def test_export_view_name_nonascii(self):
        graph = self.create_graph_and_login(graph_name="éléphant")
        url = reverse("rhizom:graph_export", kwargs=dict(
                      graph=graph.pk, name="graph", format="json"))
        response = self.client.get(url)
        self.assertEqual(
            response["Content-Disposition"],
            "attachment; filename*=UTF-8''%C3%A9l%C3%A9phant.json")
        self.assertEqual(response["Content-Type"], "application/json")
        self.assertEqual(json.loads(
            response.content.decode(response.charset))["graph"]["name"],
            "éléphant")

    def test_export_view_name_nonascii_webkit(self):
        graph = self.create_graph_and_login(graph_name="éléphant")
        url = reverse("rhizom:graph_export", kwargs=dict(
                      graph=graph.pk, name="graph", format="json"))
        try:
            response = self.client.get(url, HTTP_USER_AGENT='WebKit')
        except UnicodeDecodeError as e:
            self.fail(e)
        self.assertEqual(
            response["Content-Disposition"], "attachment; ")
        self.assertEqual(response["Content-Type"], "application/json")
        self.assertEqual(json.loads(
            response.content.decode(response.charset))["graph"]["name"],
            "éléphant")

    def test_import_export_v1(self):
        graph = Graph.objects.create(name="test")
        User.objects.create_user(username="user1", email="user1@example.com")
        User.objects.create_user(username="user2", email="user2@example.com")
        User.objects.create_user(username="user3", email="user3@example.com")
        data = {
            "graph": {
                "name": "different name",
                "anonymous": True,
                "center_id": 2,
            },
            "persons": {
                "1": {"name": "person1"},
                "2": {"name": "person2"},
                "3": {"name": "person3"},
            },
            "relationships": [
                {"source_id": 1,
                 "target_id": 2,
                 "type_name": "type1",
                 "dotted": False, },
                {"source_id": 2,
                 "target_id": 3,
                 "type_name": "type2",
                 "dotted": True, },
            ],
            "relationship_types": [
                {"name": "type1", "color": "#0000FF", },
                {"name": "type2", "color": "#FF0000", },
            ],
            "permissions": [
                {"email": "user1@example.com", "level": "admin"},
                {"email": "user2@example.com", "level": "edit"},
                {"email": "user3@example.com", "level": "view"},
            ],
            "version": 1,
        }
        graph.import_dict(data)
        graph.refresh_from_db()
        self.assertEqual(graph.name, "different name")
        self.assertTrue(graph.anonymous)
        self.assertEqual([p.name for p in graph.person_set.order_by("name")],
                         ["person1", "person2", "person3"])
        person1 = Person.objects.get(graph=graph, name="person1")
        person2 = Person.objects.get(graph=graph, name="person2")
        person3 = Person.objects.get(graph=graph, name="person3")
        self.assertEqual(graph.center, person2)
        self.assertEqual(
            [(rt.name, rt.color) for rt in
             graph.relationshiptype_set.order_by("name")],
            [("type1", "#0000FF"), ("type2", "#FF0000")]
            )
        self.assertEqual(
            [(r.source, r.target, r.reltype.name, r.dotted)
             for r in graph.relationships.order_by("source__pk")],
            [(person1, person2, "type1", False),
             (person2, person3, "type2", True)],
            )
        self.assertEqual(
            [(p.user.username, p.level) for p in
             graph.permission_set.order_by("user__username")],
            [("user1", PermissionLevel.admin.value),
             ("user2", PermissionLevel.edit.value),
             ("user3", PermissionLevel.view.value)]
            )
