from django.contrib.auth.models import User, AnonymousUser

from rhizom.auth import GraphPermBackend
from rhizom.models.graph import Graph
from rhizom.models.permission import Permission, PermissionLevel
from .utils import TestCase


class ViewsBasicTestCase(TestCase):

    def setUp(self):
        self.backend = GraphPermBackend()
        self.graph = Graph.objects.create(name="test")
        self.user = User.objects.create_user(
            username="user", password="test", is_active=True)

    def test_anonymous_view_forbidden(self):
        user = AnonymousUser()
        self.assertFalse(self.graph.allowed_for(user, "view"))

    def test_anonymous_view_allowed(self):
        user = AnonymousUser()
        Permission.objects.create(
            user=None, graph=self.graph,
            level=PermissionLevel.view.value)
        self.assertTrue(self.graph.allowed_for(user, "view"))

    def test_user_view_forbidden(self):
        self.assertFalse(self.graph.allowed_for(self.user, "view"))
        self.assertFalse(self.graph.allowed_for(self.user, "edit"))
        self.assertFalse(self.graph.allowed_for(self.user, "admin"))

    def test_user_view_allowed(self):
        Permission.objects.create(
            user=self.user, graph=self.graph,
            level=PermissionLevel.view.value)
        self.assertTrue(self.graph.allowed_for(self.user, "view"))
        self.assertFalse(self.graph.allowed_for(self.user, "edit"))
        self.assertFalse(self.graph.allowed_for(self.user, "admin"))

    def test_user_edit_allowed(self):
        Permission.objects.create(
            user=self.user, graph=self.graph,
            level=PermissionLevel.edit.value)
        self.assertTrue(self.graph.allowed_for(self.user, "view"))
        self.assertTrue(self.graph.allowed_for(self.user, "edit"))
        self.assertFalse(self.graph.allowed_for(self.user, "admin"))

    def test_user_admin_allowed(self):
        Permission.objects.create(
            user=self.user, graph=self.graph,
            level=PermissionLevel.admin.value)
        self.assertTrue(self.graph.allowed_for(self.user, "view"))
        self.assertTrue(self.graph.allowed_for(self.user, "edit"))
        self.assertTrue(self.graph.allowed_for(self.user, "admin"))

    def test_public_allowed_but_not_user(self):
        # The graph has public view access but there is no user-specific
        # permission.
        Permission.objects.create(
            user=None, graph=self.graph,
            level=PermissionLevel.view.value)
        self.assertTrue(self.graph.allowed_for(self.user, "view"))

    def test_public_edit_allowed_but_not_user(self):
        # The graph has public edit access but there is no user-specific
        # permission.
        Permission.objects.create(
            user=None, graph=self.graph,
            level=PermissionLevel.view.value)
        self.assertTrue(self.graph.allowed_for(self.user, "view"))

    def test_public_view_allowed_and_user_edit(self):
        # The graph has public view access and user edit access.
        Permission.objects.create(
            user=None, graph=self.graph,
            level=PermissionLevel.view.value)
        Permission.objects.create(
            user=self.user, graph=self.graph,
            level=PermissionLevel.edit.value)
        self.assertTrue(self.graph.allowed_for(self.user, "edit"))
