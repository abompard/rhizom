import json
from urllib.parse import urlencode

from django.contrib.auth.models import User
from django.urls import reverse

from .utils import TestCase


class AdminViewTestCase(TestCase):

    def setUp(self):
        self.graph = self.create_graph_and_login()

    def test_admin_perm_add(self):
        user = User.objects.create_user(username="newuser")
        url = reverse("rhizom:graph_admin_access", kwargs=dict(
                      graph=self.graph.pk))
        response = self.client.post(url, data={
                "user": user.username,
                "level": "1",
            })
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(
            response.content.decode(response.charset))["status"], "OK")
        self.graph.refresh_from_db()
        self.assertEqual(self.graph.permission_set.count(), 2)
        self.assertEqual(
            self.graph.permission_set.filter(
                user__username="newuser", level=1).count(),
            1)

    def test_admin_perm_edit(self):
        perm = self.graph.permission_set.first()
        url = reverse("rhizom:graph_admin_access", kwargs=dict(
                      graph=self.graph.pk, pk=perm.pk))
        response = self.client.put(url, data=urlencode({
                "level": "2",
            }))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(
            response.content.decode(response.charset))["status"], "OK")
        self.graph.refresh_from_db()
        self.assertEqual(self.graph.permission_set.count(), 1)
        self.assertEqual(
            [(p.user.username, p.level)
             for p in self.graph.permission_set.all()],
            [("user", 2)])

    def test_admin_perm_delete(self):
        perm = self.graph.permission_set.first()
        url = reverse("rhizom:graph_admin_access", kwargs=dict(
                      graph=self.graph.pk, pk=perm.pk))
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(
            response.content.decode(response.charset))["status"], "OK")
        self.graph.refresh_from_db()
        self.assertEqual(self.graph.permission_set.count(), 0)

    def test_admin_graphprops_no_center(self):
        self.graph.center_id = 1
        self.graph.save()
        url = reverse("rhizom:graph_admin", kwargs=dict(
                      graph=self.graph.pk))
        response = self.client.post(url, data={
                "formname": "graphprops",
                "graphprops-name": "Other Graph Name",
                "graphprops-center_id": "0",
                "graphprops-anonymous": "y",
            })
        self.assertRedirects(response, url)
        self.graph.refresh_from_db()
        self.assertEqual(self.graph.name, "Other Graph Name")
        self.assertEqual(self.graph.center_id, None)
        self.assertEqual(self.graph.anonymous, True)
