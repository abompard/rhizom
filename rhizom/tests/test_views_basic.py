import datetime
import json
from io import StringIO

from django.contrib.auth.models import User
from django.urls import reverse
from django.utils.timezone import utc

from rhizom.models.graph import Graph
from rhizom.models.permission import Permission, PermissionLevel
from rhizom.models.relationship import RelationshipType
from .utils import TestCase


class ViewsBasicTestCase(TestCase):

    def setUp(self):
        self.graph = self.create_graph_and_login()

    def _test_view(self, url):
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        return response

    def test_index(self):
        self._test_view(reverse("rhizom:index"))

    def test_graph_creation(self):
        response = self.client.post(reverse("rhizom:index"), {
            "formname": "new",
            "name": "dummy",
        })
        newgraph = Graph.objects.filter(name="dummy").first()
        self.assertIsNotNone(newgraph)
        self.assertRedirects(
            response, reverse("rhizom:graph_admin",
                              kwargs={"graph": newgraph.pk}))
        user = User.objects.get(username="user")
        perms = Permission.objects.filter(graph=newgraph).all()
        self.assertEqual(len(perms), 1)
        self.assertEqual(perms[0].user, user)
        self.assertEqual(perms[0].level, PermissionLevel.admin)
        reltypes = RelationshipType.objects.filter(graph=newgraph).all()
        self.assertEqual(len(reltypes), 1)
        self.assertEqual(reltypes[0].color, "#000000")

    def test_graph(self):
        old_date = datetime.datetime(2015, 1, 1, 0, 0, 0, tzinfo=utc)
        self.graph.last_access = old_date
        self.graph.save()
        self._test_view(reverse("rhizom:graph_view",
                        kwargs=dict(graph=self.graph.pk)))
        self.graph.refresh_from_db()
        self.assertTrue(
            self.graph.last_access > old_date)

    def test_data(self):
        response = self._test_view(
            reverse("rhizom:graph_data", kwargs=dict(graph=self.graph.pk)))
        self.assertEqual(response["Content-Type"], "application/json")

    def test_edit(self):
        self._test_view(
            reverse("rhizom:graph_edit", kwargs=dict(graph=self.graph.pk)))

    def test_admin(self):
        self._test_view(
            reverse("rhizom:graph_admin", kwargs=dict(graph=self.graph.pk)))

    def test_profile(self):
        self._test_view(reverse("rhizom:profile"))

    def test_export_graph(self):
        response = self._test_view(
            reverse(
                "rhizom:graph_export",
                kwargs=dict(graph=self.graph.pk, name="graph", format="json"))
            )
        self.assertEqual(response["Content-Type"], "application/json")

    def test_import_graph(self):
        data = {
            "graph": {},
            "relationship_types": [],
            "persons": [],
            "relationships": [],
            "version": 2,
        }
        graph_data = StringIO(json.dumps(data))
        response = self.client.post(
            reverse("rhizom:graph_admin", kwargs={"graph": self.graph.pk}),
            data={"formname": "graphimport", "import": graph_data})
        self.assertRedirects(response, reverse("rhizom:graph_admin",
                             kwargs=dict(graph=self.graph.pk)))

    def test_as_svg(self):
        response = self.client.post(
            reverse(
                "rhizom:graph_export",
                kwargs=dict(graph=self.graph.pk, name="graph", format="svg")),
            data={'svg': '<svg></svg>'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response["Content-Type"], "image/svg+xml; charset=utf-8")
        self.assertContains(response, "<style>")
        self.assertContains(response, "</style>")
        self.assertContains(response, ".node circle")

    def test_as_svg_get(self):
        response = self.client.get(
            reverse(
                "rhizom:graph_export",
                kwargs=dict(graph=self.graph.pk, name="graph", format="svg")
            )
        )
        self.assertRedirects(
            response, reverse("rhizom:graph_view",
                              kwargs={"graph": self.graph.pk}))
