from .utils import TestCase
from ..models.graph import Graph
from ..models.permission import Permission, PermissionLevel


class PermissionTestCase(TestCase):

    def setUp(self):
        self.graph = Graph.objects.create(name="test")

    def test_as_dict_public_view(self):
        perm = Permission.objects.create(
            graph=self.graph, user=None, level=PermissionLevel.view)
        try:
            perm_dict = perm.as_dict()
        except AttributeError as e:
            self.fail(e)
        self.assertEqual(perm_dict, {
            "user.username": None, "level": "view",
        })
