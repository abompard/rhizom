from django.contrib.auth.models import User
from django.test import TestCase as DjangoTestCase

from ..models.graph import Graph
from ..models.permission import Permission, PermissionLevel


class TestCase(DjangoTestCase):

    def create_graph_and_login(self, graph_name="test"):
        # Create a graph, a user with admin permissions on it, and log it in.
        graph = Graph.objects.create(name=graph_name)
        user = User.objects.create_user(
            username="user", password="test", is_active=True)
        Permission.objects.create(
            user=user, graph=graph, level=PermissionLevel.admin.value)
        self.client.login(username="user", password="test")
        return graph
