from django import forms
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

from rhizom.lib import kwargs_modifier
from rhizom.models.graph import Graph
from rhizom.models.permission import Permission
from rhizom.models.profile import Profile
from rhizom.models.relationship import Relationship, RelationshipType


class GraphModelForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.graph = kwargs.pop("graph")
        super().__init__(*args, **kwargs)

    def save(self, commit=True):
        instance = super().save(commit=False)
        instance.graph = self.graph
        if commit:
            instance.save()
            self.save_m2m()
        return instance


class NewGraph(forms.Form):
    name = forms.CharField(
        label=_("New graph name:"), required=True)

    def save(self):
        return Graph.objects.create(name=self.cleaned_data["name"])


class CopyGraph(forms.Form):
    existing = forms.TypedChoiceField(
        label=_("Copy existing graph:"), required=True, coerce=int)

    def save(self):
        existing = Graph.objects.get(pk=self.cleaned_data["existing"])
        graph = Graph.objects.create(
            name=_("Copy of %(name)s") % dict(name=existing.name)
            )
        graph.clone(existing)
        return graph


class NewRelationship(forms.Form):

    source = forms.CharField(
        label=_("First"), required=True, max_length=254,
        widget=forms.TextInput(attrs={'class': 'autocomplete'}))
    target = forms.CharField(
        label=_("Second"), required=True, max_length=254,
        widget=forms.TextInput(attrs={'class': 'autocomplete'}))
    rtype = forms.TypedChoiceField(label=_("Type"), required=True, coerce=int)
    dotted = forms.BooleanField(label=_("Dotted"), required=False)

    def __init__(self, *args, **kwargs):
        self.graph = kwargs.pop("graph")
        self.person = kwargs.pop("person", None)
        if self.person is not None:
            kwargs.setdefault("initial", {})["source"] = self.person.name
        super().__init__(*args, **kwargs)
        if self.person is not None:
            self.fields["source"].widget = forms.HiddenInput()
            self.fields["target"].label = _("Name")
        self.fields["rtype"].choices = [
            (rt.pk, rt.name) for rt in
            self.graph.relationshiptype_set.order_by("name")
            ]


class RelationshipForm(forms.ModelForm):
    class Meta:
        model = Relationship
        fields = ["reltype", "dotted"]

    def clean_reltype(self):
        value = self.cleaned_data["reltype"]
        if ("reltype" in self.changed_data and
            Relationship.objects.filter(
                source=self.instance.source,
                target=self.instance.target,
                reltype=value,
                ).exists()):
            raise ValidationError(
                _("This relationship already exists."), code="duplicate")
        return value


class NewAccess(GraphModelForm):

    class Meta:
        model = Permission
        fields = ["user", "level"]

    user = forms.CharField(
        label=_("Username"), required=False,
        widget=forms.TextInput(attrs={'class': 'autocomplete'}))

    def clean_user(self):
        User = get_user_model()
        if self.instance.user is not None:
            return self.instance.user  # Don't modify the user.
        value = self.cleaned_data["user"]
        if not value:
            return None
        try:
            return User.objects.get(username=value)
        except User.DoesNotExist:
            raise forms.ValidationError(
                _("This user does not exist yet. Invite them to sign up!"),
                code="notfound")


class GraphProperties(forms.ModelForm):
    class Meta:
        model = Graph
        fields = ['name', 'center', 'anonymous']
        field_classes = {
            "center": kwargs_modifier(
                forms.models.ModelChoiceField, empty_label=_("No center")
                ),
        }


class ColorInput(forms.widgets.TextInput):
    input_type = "color"


class RelationshipTypes(GraphModelForm):
    class Meta:
        model = RelationshipType
        fields = ["name", "color"]
        widgets = {
            "color": ColorInput,
        }

    def clean_name(self):
        name = self.cleaned_data["name"]
        if "name" not in self.changed_data:
            return name
        cssname = RelationshipType.to_cssname(name)
        for existing_types in self.graph.relationshiptype_set.all():
            if cssname == existing_types.cssname:
                raise ValidationError(
                    _("This name is already used."), "duplicate")
        return name


class UserProfileForm(forms.ModelForm):

    class Meta:
        model = get_user_model()
        fields = ["username", "first_name", "last_name"]

    locale = forms.ChoiceField(
        label=_("Language"), required=True, choices=Profile.LANGUAGES)

    def clean_username(self):
        value = self.cleaned_data["username"]
        User = get_user_model()
        if ("username" in self.changed_data and
                User.objects.filter(username=value).exists()):
            raise forms.ValidationError(
                _("A user with that username already exists."),
                code="unique")
        return value

    def save(self, commit=True):
        instance = super().save(commit=False)
        instance.rhizom_profile.locale = self.cleaned_data["locale"]
        if commit:
            instance.save()
            self.save_m2m()
        return instance
