import re

from django.db import models

from .utils import DumpableMixin


class Relationship(DumpableMixin, models.Model):

    source = models.ForeignKey(
        "Person", on_delete=models.CASCADE, related_name="rels_as_source")
    target = models.ForeignKey(
        "Person", on_delete=models.CASCADE, related_name="rels_as_target")
    reltype = models.ForeignKey("RelationshipType", on_delete=models.CASCADE)
    dotted = models.BooleanField(default=False)
    bidirect = models.BooleanField(default=True)

    dump_attr = (
        "source.name", "target.name", "reltype.name", "dotted", "bidirect",
        )

    def __str__(self):
        return "{} -> {}".format(self.source.name, self.target.name)


class RelationshipType(DumpableMixin, models.Model):

    graph = models.ForeignKey("Graph", on_delete=models.CASCADE)
    name = models.CharField(max_length=254, db_index=True)
    color = models.CharField(max_length=32)

    class Meta:
        ordering = ["graph", "name", "pk"]

    dump_attr = ("name", "color")

    def __str__(self):
        return self.name

    @staticmethod
    def to_cssname(name):
        return re.sub('[^a-z0-9_-]', '', name.lower())

    @property
    def cssname(self):
        return self.to_cssname(self.name)

    @property
    def rel_count(self):
        return self.relationship_set.count()
