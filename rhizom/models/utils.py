class DumpableMixin:
    """
    Allows conversion of a model object to a dict.
    The context of a given graph is assumed, thus the graph_id attribute is
    usually not included.
    """

    dump_attr = ()

    def as_dict(self):
        result = {}
        for attrname in self.dump_attr:
            value = self
            for part in attrname.split("."):
                value = getattr(value, part)
                if value is None:
                    break  # ForeignKey is NULL
            result[attrname] = value
        return result

    def from_dict(self, data):
        acceptable_fields = self.__class__._meta.get_fields()
        # Filter out relationships and auto-created fields
        acceptable_fields = [
            f.name for f in acceptable_fields
            if not f.is_relation and not f.auto_created
            ]
        for attr in self.dump_attr:
            if attr in data and attr in acceptable_fields:
                # print("Setting %s.%s to %s"
                #       % (self.__class__.__name__, attr, data[attr]))
                setattr(self, attr, data[attr])
        self.save()
