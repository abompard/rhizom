from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _
from enum import IntEnum

from .utils import DumpableMixin


class PermissionLevel(IntEnum):
    view = 1
    edit = 2
    admin = 3


def get_perm_choices():
    return [
        (getattr(PermissionLevel, l).value, n) for l, n in
        (
            ("view", _("View")),
            ("edit", _("Edit")),
            ("admin", _("Admin")),
        )
    ]


class Permission(DumpableMixin, models.Model):

    graph = models.ForeignKey("Graph", on_delete=models.CASCADE)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE,
        related_name="graph_permissions", null=True, blank=True)
    level = models.IntegerField(choices=get_perm_choices())

    class Meta:
        unique_together = ("graph", "user")
        ordering = ["graph", "level", "user__username"]

    @property
    def level_as_string(self):
        return PermissionLevel(self.level).name

    def as_dict(self):
        username = self.user.username if self.user is not None else None
        return {
            "user.username": username,
            "level": self.level_as_string,
        }

    def from_dict(self, data):
        self.level = PermissionLevel[data["level"]].value
        self.save()
