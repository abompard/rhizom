from django.db import models
from .utils import DumpableMixin


class Person(DumpableMixin, models.Model):

    name = models.CharField(max_length=254, db_index=True)
    graph = models.ForeignKey("Graph", on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    @property
    def relationships(self):
        from .relationship import Relationship
        return Relationship.objects.filter(
            models.Q(source=self) | models.Q(target=self)
        )

    dump_attr = ("name",)
