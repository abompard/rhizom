# import pytz

from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Profile(models.Model):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE,
        related_name="rhizom_profile")
    # TIMEZONES = sorted([(tz, tz) for tz in pytz.common_timezones])
    # timezone = models.CharField(
    #    max_length=100, choices=TIMEZONES, default="")
    LANGUAGES = [(k, _(v)) for k, v in settings.LANGUAGES]
    locale = models.CharField(choices=LANGUAGES, null=True, max_length=10)

    def __str__(self):
        return '<Rhizom profile for {}>'.format(self.user.username)
