from django import VERSION
from django.contrib.auth import get_user_model
from django.db import models
from django.urls import reverse
from django.utils.timezone import now
from .permission import Permission
from .person import Person
from .relationship import Relationship, RelationshipType
from .utils import DumpableMixin


graph_permissions = [
    ('admin_graph', 'Can admin the graph'),
]
if VERSION < (2, 1):
    graph_permissions.insert(0, ('view_graph', 'Can see the graph'))


class Graph(DumpableMixin, models.Model):
    name = models.CharField(max_length=254, db_index=True)
    anonymous = models.BooleanField(default=False)
    center = models.ForeignKey(
        "Person", on_delete=models.SET_NULL, blank=True, null=True,
        related_name="+")
    creation = models.DateTimeField(auto_now_add=True)
    last_access = models.DateTimeField(default=now)
    # Don't use auto_now for the mtime because somtimes only last_access is
    # updated.
    last_modification = models.DateTimeField(default=now)

    dump_attr = ("name", "anonymous", "center.name")

    class Meta:
        permissions = graph_permissions

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('rhizom:graph_view', args=[str(self.id)])

    def allowed_for(self, user, level):
        return user.has_perm("rhizom.{}_graph".format(level), self)

    @property
    def relationships(self):
        from rhizom.models.relationship import Relationship
        return Relationship.objects.filter(
            models.Q(source__graph=self) | models.Q(target__graph=self)
            ).distinct()

    # Data manipulation
    def clone(self, graph):
        for perm in graph.permission_set.all():
            self.permission_set.create(
                user=perm.user, level=perm.level)
        for other_reltype in graph.relationshiptype_set.all():
            self.relationshiptype_set.create(
                name=other_reltype.name, color=other_reltype.color)
        for other_person in graph.person_set.all():
            self.person_set.create(name=other_person.name)
        for other_rel in graph.relationships:
            Relationship.objects.create(
                source=self.person_set.get(name=other_rel.source.name),
                target=self.person_set.get(name=other_rel.target.name),
                reltype=self.relationshiptype_set.get(
                    name=other_rel.reltype.name),
                )
        self.anonymous = graph.anonymous
        if graph.center is not None:
            self.center = self.person_set.get(name=graph.center.name)
        self.save()

    def export_dict(self, with_perms=False):
        data = {
            "version": 2,
            "graph": self.as_dict(),
            "relationship_types": [],
            "persons": [],
            "relationships": [],
        }
        for reltype in self.relationshiptype_set.all():
            data["relationship_types"].append(reltype.as_dict())
        for person in self.person_set.all():
            data["persons"].append(person.as_dict())
            for rel in Relationship.objects.filter(
                    models.Q(source=person) | models.Q(target=person)
                    ):
                rel_dict = rel.as_dict()
                if rel_dict not in data["relationships"]:
                    data["relationships"].append(rel_dict)
        if with_perms:
            data["permissions"] = []
            for perm in self.permission_set.all():
                data["permissions"].append(perm.as_dict())
        return data

    def import_dict(self, data):
        if data.get("version") == 2:
            return self._import_dict_v2(data)
        elif data.get("version") == 1:
            return self._import_dict_v1(data)
        raise ValueError

    def _import_dict_v2(self, data):
        self.from_dict(data["graph"])
        for reltype_data in data["relationship_types"]:
            reltype, _created = RelationshipType.objects.get_or_create(
                graph=self, name=reltype_data["name"])
            reltype.from_dict(reltype_data)
        for person_data in data["persons"]:
            person, _created = Person.objects.get_or_create(
                graph=self, name=person_data["name"])
            person.from_dict(person_data)
        center_name = data["graph"].get("center.name")
        if center_name:
            self.center = self.person_set.get(name=center_name)
            self.save()
        for rel_data in data["relationships"]:
            source_name = rel_data["source.name"]
            target_name = rel_data["target.name"]
            reltype = self.relationshiptype_set.get(
                name=rel_data["reltype.name"])
            rel, _created = Relationship.objects.get_or_create(
                source=self.person_set.get(name=source_name),
                target=self.person_set.get(name=target_name),
                reltype=reltype)
            rel.from_dict(rel_data)
        if "permissions" in data:
            for perm_data in data["permissions"]:
                try:
                    perm = self.permission_set.get(
                        user__username=perm_data["user.username"])
                except Permission.DoesNotExist:
                    User = get_user_model()
                    try:
                        user = User.objects.get(
                            username=perm_data["user.username"])
                    except User.DoesNotExist:
                        continue
                    perm = Permission(graph=self, user=user)
                perm.from_dict(perm_data)

    def _import_dict_v1(self, data):
        self.from_dict(data["graph"])
        for reltype_data in data["relationship_types"]:
            reltype, _created = RelationshipType.objects.get_or_create(
                graph=self, name=reltype_data["name"])
            reltype.from_dict(reltype_data)
        for person_id_orig in sorted(data["persons"], key=int):
            person_data = data["persons"][person_id_orig]
            person, _created = Person.objects.get_or_create(
                graph=self, name=person_data["name"])
            person.from_dict(person_data)
        if data["graph"].get("center_id"):
            center_id = data["graph"]["center_id"]
            center_name = data["persons"][str(center_id)]["name"]
            self.center = self.person_set.get(name=center_name)
            self.save()
        for rel_data in data["relationships"]:
            source_name = data["persons"][str(rel_data["source_id"])]["name"]
            target_name = data["persons"][str(rel_data["target_id"])]["name"]
            reltype = self.relationshiptype_set.get(
                name=rel_data["type_name"])
            rel, _created = Relationship.objects.get_or_create(
                source=self.person_set.get(name=source_name),
                target=self.person_set.get(name=target_name),
                reltype=reltype)
            rel.from_dict(rel_data)
        if "permissions" in data:
            for perm_data in data["permissions"]:
                try:
                    perm = self.permission_set.get(
                        user__email=perm_data["email"])
                except Permission.DoesNotExist:
                    User = get_user_model()
                    try:
                        user = User.objects.get(
                            email=perm_data["email"])
                    except User.DoesNotExist:
                        continue
                    perm = Permission(graph=self, user=user)
                perm.from_dict(perm_data)
