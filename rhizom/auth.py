from .models.graph import Graph
from .models.permission import Permission, PermissionLevel


class GraphPermBackend:

    def get_user(self, user_id):
        return None

    def authenticate(self, **kwargs):
        return None

    def _get_user_perm_level(self, user, obj=None):
        try:
            perm = Permission.objects.get(user=user, graph=obj)
        except Permission.DoesNotExist:
            return 0
        return PermissionLevel(perm.level)

    def get_all_permissions(self, user, obj=None):
        if not isinstance(obj, Graph):
            return set()
        # This request is for us
        public_level = self._get_user_perm_level(None, obj)
        if user.is_authenticated and user.is_active:
            level = self._get_user_perm_level(user, obj)
        else:
            level = 0
        level = max(public_level, level)
        result = set()
        for available_level in PermissionLevel:
            if available_level.value == 0:
                continue
            if available_level <= level:
                result.add("rhizom.{}_graph".format(available_level.name))
        return result

    def has_perm(self, user, perm, obj=None):
        return perm in self.get_all_permissions(user, obj)
