from django.apps import AppConfig


class RhizomConfig(AppConfig):
    name = 'rhizom'
    verbose_name = 'Rhizom'

    def ready(self):
        import rhizom.signals  # noqa
