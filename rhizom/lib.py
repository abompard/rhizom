from functools import wraps


def get_relationship_ids(nodeid, links):
    rel_ids = set()
    for link in links:
        if link["source"] == nodeid:
            rel_ids.add(link["target"])
        if link["target"] == nodeid:
            rel_ids.add(link["source"])
    return rel_ids


def compute_node_circles(nodes, links, center_index):
    def compute_circle(circle):
        if circle > 30:
            return  # Recursion failsafe
        next_nodes = set()
        for nodeid, node in enumerate(nodes):
            if circle is not None and node.get("circle") != circle:
                continue  # only consider the current circle
            next_nodes.update(get_relationship_ids(nodeid, links))
        next_nodes = [n for n in next_nodes if "circle" not in nodes[n]]
        if not next_nodes:
            return  # nothing left to set
        for nodeid in next_nodes:
            nodes[nodeid]["circle"] = circle + 1
        compute_circle(circle + 1)
    nodes[center_index]["circle"] = 0
    compute_circle(0)


def compute_node_branch(nodes, links):
    def compute_branch(circle):
        if circle > 30:
            return  # Recursion failsafe
        next_nodes_ids = [
            n["id"] for n in nodes
            if "circle" in n and n["circle"] == circle
            ]
        for nodeid, node in enumerate(nodes):
            if node["id"] not in next_nodes_ids or "branch" in node:
                continue
            branch = None
            for rel_id in get_relationship_ids(nodeid, links):
                rel_branch = nodes[rel_id].get("branch")
                if rel_branch is not None:
                    # TODO: allow being on multiple branches
                    branch = rel_branch
            assert branch is not None, "No branch found for %s" % node["name"]
            node["branch"] = branch
        compute_branch(circle + 1)
    first_circle = [n for n in nodes if "circle" in n and n["circle"] == 1]
    for node in first_circle:
        node["branch"] = node["name"]
    compute_branch(2)


def kwargs_modifier(func, **kwargs):
    @wraps(func)
    def caller(*args, **func_kw):
        func_kw.update(kwargs)
        return func(*args, **func_kw)
    return caller
