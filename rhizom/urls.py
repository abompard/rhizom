from django.conf.urls import include, url

from .views import index, profile, graph, admin, edit

app_name = "rhizom"

graph_patterns = [
    url(r'^$', graph.GraphView.as_view(), name='graph_view'),
    url(r'^data.js$', graph.GraphData.as_view(), name='graph_data'),
    url(r'^export/(?P<name>.+)\.(?P<format>[a-z]+)$',
        graph.GraphExport.as_view(), name='graph_export'),
    url(r'^edit/$', edit.GraphEditAll.as_view(), name='graph_edit'),
    url(r'^edit/person/(?P<person>\d+)$', edit.GraphEditPerson.as_view(),
        name='graph_edit_person'),
    url(r'^edit/relationships/$', edit.GraphRelAdd.as_view(),
        name='graph_edit_relationship_add'),
    url(r'^edit/relationships/(?P<rel>\d+)$', edit.GraphRelEdit.as_view(),
        name='graph_edit_relationship_edit'),
    url(r'^admin/$', admin.GraphAdmin.as_view(), name='graph_admin'),
    url(r'^admin/reltype/$', admin.GraphRelType.as_view(),
        name='graph_admin_reltypes'),
    url(r'^admin/reltype/(?P<pk>\d+)$', admin.GraphRelType.as_view(),
        name='graph_admin_reltypes'),
    url(r'^admin/access/$', admin.GraphAccess.as_view(),
        name='graph_admin_access'),
    url(r'^admin/access/(?P<pk>\d+)$', admin.GraphAccess.as_view(),
        name='graph_admin_access'),
    url(r'^admin/find-username$', admin.FindUsername.as_view(),
        name='graph_admin_find_username'),
]

urlpatterns = [
    url(r'^$', index.IndexView.as_view(), name="index"),
    url(r'^profile/$', profile.ProfileView.as_view(), name="profile"),
    url(r'^graph/(?P<graph>\d+)/', include(graph_patterns)),
    url(r'^pages/?', include('django.contrib.flatpages.urls')),
]
