Rhizom
======

Rhizom is a web application that can display efficiently a network of relationships.

It is based on Python 3, `Django`_, and the graph is displayed using `D3.js`_.

- Download it from PyPI: https://pypi.python.org/pypi/rhizom
- Get the source and report bugs on GitLab: https://gitlab.com/abompard/rhizom

Rhizom is licensed under the `Affero GPL v3`_ or any later version.

.. _Django: https://djangoproject.com
.. _D3.js: http://d3js.org/
.. _`Mozilla Persona`: http://persona.org
.. _`Affero GPL v3`: http://www.gnu.org/licenses/agpl-3.0.html


Installation
------------

This is how you can quickly check out Rhizom. Those steps are not fit for a
proper production deployment.

Install Rhizom's dependencies. Do do that you can either use your
distribution's package manager or create a Python VirtualEnv with the following
commands::

    $ pyvenv venv
    $ source ./venv/bin/activate
    $ python setup.py develop

If you have an existing Django project, add "rhizom" to your ``INSTALLED_APPS``
setting. You will also have to include the rhizom URLconf in your project
urls.py like this::

    url(r'^rhizom/', include('rhizom.urls')),

If you don't have an existing Django project, an example ``settings.py`` file is
provided in the ``example_project`` directory. Edit it as necessary.

Run ``python manage.py migrate`` to create the rhizom models.

Run ``python manage.py runserver`` to start the development server, and visit
http://127.0.0.1:8000/rhizom/ to create graphs.

If you are unfamiliar with the way Django applications are usually deployed,
check out `the official documentation`_ on Django's website.

.. _`the official documentation`: https://docs.djangoproject.com

Rhizom provides some configuration examples to help you get started with common
deployment cases, check out the ``deploy`` subdirectory.

I hope you'll like it. Feedback is very welcome!


Contributing to the project
---------------------------

If you like Rhizom and want to help the project, you can do so in the following manner (in no particular order):

- installing and testing: see Quickstart above, report bugs on the Gitlab project page.
- fixing bugs and adding features: checkout the code and use merge requests.
- documentation: if things seem unclear or could be explained better, please do so.
- design: if you think the UI could be made more intuitive, I'm very open to suggestions.
- translations: Rhizom is currently translated into English, French, and Catalan. If you want to add a new translation or join a translation team, please contact us.
- spreading the word: if you like Rhizom, tell your friends! :-)
