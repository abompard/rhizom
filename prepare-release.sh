#!/bin/bash

set -e

sh makemessages.sh
./manage.py compilemessages
tox
echo
echo "==> Ready to release!"
